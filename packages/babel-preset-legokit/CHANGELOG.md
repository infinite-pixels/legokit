# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.33](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.32...babel-preset-legokit@1.2.33) (2019-08-11)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.32](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.31...babel-preset-legokit@1.2.32) (2019-08-11)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.31](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.30...babel-preset-legokit@1.2.31) (2019-08-11)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.30](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.29...babel-preset-legokit@1.2.30) (2019-08-11)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.29](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.29...babel-preset-legokit@1.2.29) (2019-08-11)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.29](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.28...babel-preset-legokit@1.2.29) (2019-08-11)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.28](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.27...babel-preset-legokit@1.2.28) (2019-08-05)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.27](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.26...babel-preset-legokit@1.2.27) (2019-08-05)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.26](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.25...babel-preset-legokit@1.2.26) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.25](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.24...babel-preset-legokit@1.2.25) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.24](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.23...babel-preset-legokit@1.2.24) (2019-08-04)


### Bug Fixes

* react config ([2fceca7](https://gitlab.com/infinite-pixels/legokit/commit/2fceca7))





## [1.2.23](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.22...babel-preset-legokit@1.2.23) (2019-08-04)


### Bug Fixes

* reorder ([6bfff8b](https://gitlab.com/infinite-pixels/legokit/commit/6bfff8b))





## [1.2.22](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.21...babel-preset-legokit@1.2.22) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.21](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.20...babel-preset-legokit@1.2.21) (2019-08-04)


### Bug Fixes

* preset babel ([d3f180c](https://gitlab.com/infinite-pixels/legokit/commit/d3f180c))





## [1.2.20](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.19...babel-preset-legokit@1.2.20) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.19](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.18...babel-preset-legokit@1.2.19) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.18](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.17...babel-preset-legokit@1.2.18) (2019-08-04)


### Bug Fixes

* mispelled babel plugin ([4390711](https://gitlab.com/infinite-pixels/legokit/commit/4390711))





## [1.2.17](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.16...babel-preset-legokit@1.2.17) (2019-08-04)


### Bug Fixes

* testing utils ([43530d4](https://gitlab.com/infinite-pixels/legokit/commit/43530d4))





## [1.2.16](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.15...babel-preset-legokit@1.2.16) (2019-08-04)


### Bug Fixes

* hopefully ([adeeca5](https://gitlab.com/infinite-pixels/legokit/commit/adeeca5))





## [1.2.15](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.14...babel-preset-legokit@1.2.15) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.14](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.13...babel-preset-legokit@1.2.14) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.13](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.12...babel-preset-legokit@1.2.13) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.12](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.11...babel-preset-legokit@1.2.12) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.11](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.10...babel-preset-legokit@1.2.11) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.10](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.9...babel-preset-legokit@1.2.10) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.9](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.8...babel-preset-legokit@1.2.9) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.8](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.7...babel-preset-legokit@1.2.8) (2019-08-04)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.7](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.6...babel-preset-legokit@1.2.7) (2019-08-03)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.6](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.5...babel-preset-legokit@1.2.6) (2019-08-03)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.5](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.4...babel-preset-legokit@1.2.5) (2019-08-03)


### Reverts

* back to preconstruct ([d86df4f](https://gitlab.com/infinite-pixels/legokit/commit/d86df4f))





## [1.2.4](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.3...babel-preset-legokit@1.2.4) (2019-08-03)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.3](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.2...babel-preset-legokit@1.2.3) (2019-08-03)

**Note:** Version bump only for package babel-preset-legokit





## [1.2.2](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.1...babel-preset-legokit@1.2.2) (2019-08-03)


### Bug Fixes

* **babel-preset-legokit:** set modules to false in preset-env config ([ee17872](https://gitlab.com/infinite-pixels/legokit/commit/ee17872))





## [1.2.1](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.2.0...babel-preset-legokit@1.2.1) (2019-08-03)

**Note:** Version bump only for package babel-preset-legokit





# [1.2.0](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.1.3...babel-preset-legokit@1.2.0) (2019-08-03)


### Features

* add custom eslint config module ([13ddb56](https://gitlab.com/infinite-pixels/legokit/commit/13ddb56))





## [1.1.3](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.1.2...babel-preset-legokit@1.1.3) (2019-08-02)

**Note:** Version bump only for package babel-preset-legokit





## [1.1.2](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.1.1...babel-preset-legokit@1.1.2) (2019-08-02)

**Note:** Version bump only for package babel-preset-legokit





## [1.1.1](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.1.0...babel-preset-legokit@1.1.1) (2019-08-02)

**Note:** Version bump only for package babel-preset-legokit





# [1.1.0](https://gitlab.com/infinite-pixels/legokit/compare/babel-preset-legokit@1.0.4...babel-preset-legokit@1.1.0) (2019-08-02)


### Features

* remove changelogs from dependencies ([f3a0405](https://gitlab.com/infinite-pixels/legokit/commit/f3a0405))
