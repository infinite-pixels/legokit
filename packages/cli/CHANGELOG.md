# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.7.128](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.127...@legokit/cli@0.7.128) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.127](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.126...@legokit/cli@0.7.127) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.126](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.125...@legokit/cli@0.7.126) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.125](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.124...@legokit/cli@0.7.125) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.124](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.123...@legokit/cli@0.7.124) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.123](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.122...@legokit/cli@0.7.123) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.122](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.121...@legokit/cli@0.7.122) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.121](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.120...@legokit/cli@0.7.121) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.120](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.119...@legokit/cli@0.7.120) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.119](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.118...@legokit/cli@0.7.119) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.118](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.117...@legokit/cli@0.7.118) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.117](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.116...@legokit/cli@0.7.117) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.116](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.115...@legokit/cli@0.7.116) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.115](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.114...@legokit/cli@0.7.115) (2019-08-13)

**Note:** Version bump only for package @legokit/cli





## [0.7.114](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.113...@legokit/cli@0.7.114) (2019-08-12)

**Note:** Version bump only for package @legokit/cli





## [0.7.113](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.112...@legokit/cli@0.7.113) (2019-08-12)

**Note:** Version bump only for package @legokit/cli





## [0.7.112](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.111...@legokit/cli@0.7.112) (2019-08-11)

**Note:** Version bump only for package @legokit/cli





## [0.7.111](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.110...@legokit/cli@0.7.111) (2019-08-11)

**Note:** Version bump only for package @legokit/cli





## [0.7.110](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.109...@legokit/cli@0.7.110) (2019-08-11)

**Note:** Version bump only for package @legokit/cli





## [0.7.109](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.108...@legokit/cli@0.7.109) (2019-08-11)

**Note:** Version bump only for package @legokit/cli





## [0.7.108](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.108...@legokit/cli@0.7.108) (2019-08-11)

**Note:** Version bump only for package @legokit/cli





## [0.7.108](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.107...@legokit/cli@0.7.108) (2019-08-11)

**Note:** Version bump only for package @legokit/cli





## [0.7.107](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.106...@legokit/cli@0.7.107) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.106](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.105...@legokit/cli@0.7.106) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.105](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.104...@legokit/cli@0.7.105) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.104](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.103...@legokit/cli@0.7.104) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.103](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.102...@legokit/cli@0.7.103) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.102](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.101...@legokit/cli@0.7.102) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.101](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.100...@legokit/cli@0.7.101) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.100](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.99...@legokit/cli@0.7.100) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.99](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.98...@legokit/cli@0.7.99) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.98](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.97...@legokit/cli@0.7.98) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.97](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.96...@legokit/cli@0.7.97) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.96](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.95...@legokit/cli@0.7.96) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.95](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.94...@legokit/cli@0.7.95) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.94](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.93...@legokit/cli@0.7.94) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.93](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.92...@legokit/cli@0.7.93) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.92](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.91...@legokit/cli@0.7.92) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.91](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.90...@legokit/cli@0.7.91) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.90](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.89...@legokit/cli@0.7.90) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.89](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.88...@legokit/cli@0.7.89) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.88](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.87...@legokit/cli@0.7.88) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.87](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.86...@legokit/cli@0.7.87) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.86](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.85...@legokit/cli@0.7.86) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.85](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.84...@legokit/cli@0.7.85) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.84](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.83...@legokit/cli@0.7.84) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.83](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.82...@legokit/cli@0.7.83) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.82](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.81...@legokit/cli@0.7.82) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.81](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.80...@legokit/cli@0.7.81) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.80](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.79...@legokit/cli@0.7.80) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.79](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.78...@legokit/cli@0.7.79) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.78](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.77...@legokit/cli@0.7.78) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.77](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.76...@legokit/cli@0.7.77) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.76](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.75...@legokit/cli@0.7.76) (2019-08-05)

**Note:** Version bump only for package @legokit/cli





## [0.7.75](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.74...@legokit/cli@0.7.75) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.74](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.73...@legokit/cli@0.7.74) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.73](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.72...@legokit/cli@0.7.73) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.72](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.71...@legokit/cli@0.7.72) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.71](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.70...@legokit/cli@0.7.71) (2019-08-04)


### Bug Fixes

* add coverage ([8caf7b3](https://gitlab.com/infinite-pixels/legokit/commit/8caf7b3))





## [0.7.70](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.69...@legokit/cli@0.7.70) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.69](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.68...@legokit/cli@0.7.69) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.68](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.67...@legokit/cli@0.7.68) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.67](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.66...@legokit/cli@0.7.67) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.66](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.65...@legokit/cli@0.7.66) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.65](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.64...@legokit/cli@0.7.65) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.64](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.63...@legokit/cli@0.7.64) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.63](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.62...@legokit/cli@0.7.63) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.62](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.61...@legokit/cli@0.7.62) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.61](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.60...@legokit/cli@0.7.61) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.60](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.59...@legokit/cli@0.7.60) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.59](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.58...@legokit/cli@0.7.59) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.58](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.57...@legokit/cli@0.7.58) (2019-08-04)


### Bug Fixes

* add transformer ([c933f0a](https://gitlab.com/infinite-pixels/legokit/commit/c933f0a))





## [0.7.57](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.56...@legokit/cli@0.7.57) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.56](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.55...@legokit/cli@0.7.56) (2019-08-04)


### Bug Fixes

* setup file ([cba3b43](https://gitlab.com/infinite-pixels/legokit/commit/cba3b43))





## [0.7.55](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.54...@legokit/cli@0.7.55) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.54](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.53...@legokit/cli@0.7.54) (2019-08-04)


### Bug Fixes

* jest config ([306604d](https://gitlab.com/infinite-pixels/legokit/commit/306604d))





## [0.7.53](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.52...@legokit/cli@0.7.53) (2019-08-04)


### Bug Fixes

* jest ([0e0098b](https://gitlab.com/infinite-pixels/legokit/commit/0e0098b))





## [0.7.52](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.51...@legokit/cli@0.7.52) (2019-08-04)


### Bug Fixes

* jest config ([2fcb045](https://gitlab.com/infinite-pixels/legokit/commit/2fcb045))





## [0.7.51](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.50...@legokit/cli@0.7.51) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.50](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.49...@legokit/cli@0.7.50) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.49](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.48...@legokit/cli@0.7.49) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.48](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.47...@legokit/cli@0.7.48) (2019-08-04)


### Bug Fixes

* hopefully ([adeeca5](https://gitlab.com/infinite-pixels/legokit/commit/adeeca5))





## [0.7.47](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.46...@legokit/cli@0.7.47) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.46](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.45...@legokit/cli@0.7.46) (2019-08-04)


### Bug Fixes

* missing imports in eslint plug ([6ed2c94](https://gitlab.com/infinite-pixels/legokit/commit/6ed2c94))





## [0.7.45](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.44...@legokit/cli@0.7.45) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.44](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.43...@legokit/cli@0.7.44) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.43](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.42...@legokit/cli@0.7.43) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.42](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.41...@legokit/cli@0.7.42) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.41](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.40...@legokit/cli@0.7.41) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.40](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.39...@legokit/cli@0.7.40) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.39](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.38...@legokit/cli@0.7.39) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.38](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.37...@legokit/cli@0.7.38) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.37](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.36...@legokit/cli@0.7.37) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.36](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.35...@legokit/cli@0.7.36) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.35](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.34...@legokit/cli@0.7.35) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.34](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.33...@legokit/cli@0.7.34) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.33](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.32...@legokit/cli@0.7.33) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.32](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.31...@legokit/cli@0.7.32) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.31](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.30...@legokit/cli@0.7.31) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.30](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.29...@legokit/cli@0.7.30) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.29](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.28...@legokit/cli@0.7.29) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.28](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.28...@legokit/cli@0.7.28) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.28](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.27...@legokit/cli@0.7.28) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.27](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.26...@legokit/cli@0.7.27) (2019-08-04)


### Bug Fixes

* add split chunks optimization ([f993a1c](https://gitlab.com/infinite-pixels/legokit/commit/f993a1c))





## [0.7.26](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.25...@legokit/cli@0.7.26) (2019-08-04)

**Note:** Version bump only for package @legokit/cli





## [0.7.25](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.24...@legokit/cli@0.7.25) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.24](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.23...@legokit/cli@0.7.24) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.23](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.22...@legokit/cli@0.7.23) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.22](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.21...@legokit/cli@0.7.22) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.21](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.20...@legokit/cli@0.7.21) (2019-08-03)


### Reverts

* back to preconstruct ([d86df4f](https://gitlab.com/infinite-pixels/legokit/commit/d86df4f))





## [0.7.20](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.19...@legokit/cli@0.7.20) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.19](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.18...@legokit/cli@0.7.19) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.18](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.17...@legokit/cli@0.7.18) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.17](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.16...@legokit/cli@0.7.17) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.16](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.15...@legokit/cli@0.7.16) (2019-08-03)


### Bug Fixes

* remove measure webpack plugin ([99c9ac7](https://gitlab.com/infinite-pixels/legokit/commit/99c9ac7))





## [0.7.15](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.14...@legokit/cli@0.7.15) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.14](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.13...@legokit/cli@0.7.14) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.13](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.12...@legokit/cli@0.7.13) (2019-08-03)


### Bug Fixes

* dll plugin path ([036d296](https://gitlab.com/infinite-pixels/legokit/commit/036d296))





## [0.7.12](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.11...@legokit/cli@0.7.12) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.11](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.10...@legokit/cli@0.7.11) (2019-08-03)


### Bug Fixes

* move dll plugin to web and dev ([4a23bd1](https://gitlab.com/infinite-pixels/legokit/commit/4a23bd1))





## [0.7.10](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.9...@legokit/cli@0.7.10) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.9](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.8...@legokit/cli@0.7.9) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.8](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.7...@legokit/cli@0.7.8) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.7](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.6...@legokit/cli@0.7.7) (2019-08-03)


### Bug Fixes

* cli entry point ([f8d6790](https://gitlab.com/infinite-pixels/legokit/commit/f8d6790))





## [0.7.6](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.5...@legokit/cli@0.7.6) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.5](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.4...@legokit/cli@0.7.5) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.4](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.3...@legokit/cli@0.7.4) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.7.3](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.2...@legokit/cli@0.7.3) (2019-08-03)


### Bug Fixes

* specify eslint config as string and check ([ba453e4](https://gitlab.com/infinite-pixels/legokit/commit/ba453e4))





## [0.7.2](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.1...@legokit/cli@0.7.2) (2019-08-03)


### Bug Fixes

* add default import ([33706e2](https://gitlab.com/infinite-pixels/legokit/commit/33706e2))





## [0.7.1](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.7.0...@legokit/cli@0.7.1) (2019-08-03)


### Bug Fixes

* remove relative eslintrc file ([969bbee](https://gitlab.com/infinite-pixels/legokit/commit/969bbee))





# [0.7.0](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.16...@legokit/cli@0.7.0) (2019-08-03)


### Features

* add custom eslint config module ([13ddb56](https://gitlab.com/infinite-pixels/legokit/commit/13ddb56))





## [0.6.16](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.15...@legokit/cli@0.6.16) (2019-08-03)


### Bug Fixes

* replace console statements with toolbox print ([e65af69](https://gitlab.com/infinite-pixels/legokit/commit/e65af69))





## [0.6.15](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.14...@legokit/cli@0.6.15) (2019-08-03)


### Bug Fixes

* remove clean webpack plugin and utilize command extensions ([9422b42](https://gitlab.com/infinite-pixels/legokit/commit/9422b42))





## [0.6.14](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.13...@legokit/cli@0.6.14) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.6.13](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.12...@legokit/cli@0.6.13) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.6.12](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.11...@legokit/cli@0.6.12) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.6.11](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.10...@legokit/cli@0.6.11) (2019-08-03)


### Bug Fixes

* add option to clean webpack plugin ([ab0cdaf](https://gitlab.com/infinite-pixels/legokit/commit/ab0cdaf))





## [0.6.10](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.9...@legokit/cli@0.6.10) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.6.9](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.8...@legokit/cli@0.6.9) (2019-08-03)


### Bug Fixes

* order of clean plugin ([4342eb5](https://gitlab.com/infinite-pixels/legokit/commit/4342eb5))





## [0.6.8](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.7...@legokit/cli@0.6.8) (2019-08-03)


### Bug Fixes

* add node_env in respective commands ([a057080](https://gitlab.com/infinite-pixels/legokit/commit/a057080))





## [0.6.7](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.6...@legokit/cli@0.6.7) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.6.6](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.5...@legokit/cli@0.6.6) (2019-08-03)

**Note:** Version bump only for package @legokit/cli





## [0.6.5](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.4...@legokit/cli@0.6.5) (2019-08-03)


### Bug Fixes

* add terser and compression webpack plugins ([cb7f934](https://gitlab.com/infinite-pixels/legokit/commit/cb7f934))





## [0.6.4](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.3...@legokit/cli@0.6.4) (2019-08-03)


### Bug Fixes

* loadable stats.json file path ([0650fb8](https://gitlab.com/infinite-pixels/legokit/commit/0650fb8))





## [0.6.3](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.2...@legokit/cli@0.6.3) (2019-08-03)


### Bug Fixes

* remove async function ([552cdb2](https://gitlab.com/infinite-pixels/legokit/commit/552cdb2))





## [0.6.2](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.1...@legokit/cli@0.6.2) (2019-08-03)


### Bug Fixes

* env config ([d5d1cb1](https://gitlab.com/infinite-pixels/legokit/commit/d5d1cb1))





## [0.6.1](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.6.0...@legokit/cli@0.6.1) (2019-08-03)


### Bug Fixes

* load env vars before build ([c18e890](https://gitlab.com/infinite-pixels/legokit/commit/c18e890))





# [0.6.0](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.10...@legokit/cli@0.6.0) (2019-08-03)


### Features

* add build command ([280009e](https://gitlab.com/infinite-pixels/legokit/commit/280009e))





## [0.5.10](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.9...@legokit/cli@0.5.10) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





## [0.5.9](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.8...@legokit/cli@0.5.9) (2019-08-02)


### Bug Fixes

* add parser options to eslint ([15d2b8b](https://gitlab.com/infinite-pixels/legokit/commit/15d2b8b))





## [0.5.8](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.7...@legokit/cli@0.5.8) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





## [0.5.7](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.6...@legokit/cli@0.5.7) (2019-08-02)


### Bug Fixes

* remove assets webpack plugin ([29221b7](https://gitlab.com/infinite-pixels/legokit/commit/29221b7))





## [0.5.6](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.5...@legokit/cli@0.5.6) (2019-08-02)


### Bug Fixes

* stats.json path ([b3b71de](https://gitlab.com/infinite-pixels/legokit/commit/b3b71de))





## [0.5.5](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.4...@legokit/cli@0.5.5) (2019-08-02)


### Bug Fixes

* remove dll plugin ([f73042e](https://gitlab.com/infinite-pixels/legokit/commit/f73042e))
* remove dll plugin ([4472a5b](https://gitlab.com/infinite-pixels/legokit/commit/4472a5b))





## [0.5.4](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.3...@legokit/cli@0.5.4) (2019-08-02)


### Bug Fixes

* remove react-router-dom from dll list ([76399bb](https://gitlab.com/infinite-pixels/legokit/commit/76399bb))





## [0.5.3](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.2...@legokit/cli@0.5.3) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





## [0.5.2](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.1...@legokit/cli@0.5.2) (2019-08-02)


### Bug Fixes

* autodll paths ([9cee3bd](https://gitlab.com/infinite-pixels/legokit/commit/9cee3bd))





## [0.5.1](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.5.0...@legokit/cli@0.5.1) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





# [0.5.0](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.4.2...@legokit/cli@0.5.0) (2019-08-02)


### Features

* add clean webpack plugin and remove css modules and css support ([835c6d0](https://gitlab.com/infinite-pixels/legokit/commit/835c6d0))





## [0.4.2](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.4.1...@legokit/cli@0.4.2) (2019-08-02)


### Bug Fixes

* get rid of config files ([8af77ca](https://gitlab.com/infinite-pixels/legokit/commit/8af77ca))





## [0.4.1](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.4.0...@legokit/cli@0.4.1) (2019-08-02)


### Bug Fixes

* remove friendly errors plugin ([9e4738b](https://gitlab.com/infinite-pixels/legokit/commit/9e4738b))





# [0.4.0](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.3.0...@legokit/cli@0.4.0) (2019-08-02)


### Features

* add default loadable webpack plugin ([7f16985](https://gitlab.com/infinite-pixels/legokit/commit/7f16985))





# [0.3.0](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.2.2...@legokit/cli@0.3.0) (2019-08-02)


### Features

* add friendly errors and simple progress plugin ([ba04542](https://gitlab.com/infinite-pixels/legokit/commit/ba04542))





## [0.2.2](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.2.1...@legokit/cli@0.2.2) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





## [0.2.1](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.2.0...@legokit/cli@0.2.1) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





# [0.2.0](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.13...@legokit/cli@0.2.0) (2019-08-02)


### Features

* add webpackbar ([611e1b0](https://gitlab.com/infinite-pixels/legokit/commit/611e1b0))





## [0.1.13](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.12...@legokit/cli@0.1.13) (2019-08-02)


### Bug Fixes

* add babel preset ([67fb62d](https://gitlab.com/infinite-pixels/legokit/commit/67fb62d))





## [0.1.12](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.11...@legokit/cli@0.1.12) (2019-08-02)


### Bug Fixes

* call imported plugs ([8b8d3a9](https://gitlab.com/infinite-pixels/legokit/commit/8b8d3a9))





## [0.1.11](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.10...@legokit/cli@0.1.11) (2019-08-02)


### Bug Fixes

* eslint issue ([d83d030](https://gitlab.com/infinite-pixels/legokit/commit/d83d030))





## [0.1.10](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.9...@legokit/cli@0.1.10) (2019-08-02)


### Bug Fixes

* eslintrc path ([88e67fa](https://gitlab.com/infinite-pixels/legokit/commit/88e67fa))





## [0.1.9](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.8...@legokit/cli@0.1.9) (2019-08-02)


### Bug Fixes

* add missing eslintrc file ([9c58dae](https://gitlab.com/infinite-pixels/legokit/commit/9c58dae))





## [0.1.8](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.7...@legokit/cli@0.1.8) (2019-08-02)


### Bug Fixes

* refactor plugs to es modules ([62adb2d](https://gitlab.com/infinite-pixels/legokit/commit/62adb2d))
* refactor plugs to es modules ([7f1ccb5](https://gitlab.com/infinite-pixels/legokit/commit/7f1ccb5))





## [0.1.7](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.6...@legokit/cli@0.1.7) (2019-08-02)


### Bug Fixes

* get appkktrc from paths ([424aa7c](https://gitlab.com/infinite-pixels/legokit/commit/424aa7c))





## [0.1.6](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.5...@legokit/cli@0.1.6) (2019-08-02)


### Bug Fixes

* convert utils to es6 modules ([b7aebe1](https://gitlab.com/infinite-pixels/legokit/commit/b7aebe1))
* missing kktrc module ([ebd1501](https://gitlab.com/infinite-pixels/legokit/commit/ebd1501))





## [0.1.5](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.4...@legokit/cli@0.1.5) (2019-08-02)


### Bug Fixes

* add missing utils ([f821c5c](https://gitlab.com/infinite-pixels/legokit/commit/f821c5c))





## [0.1.4](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.3...@legokit/cli@0.1.4) (2019-08-02)


### Bug Fixes

* missing module error ([9d2ea26](https://gitlab.com/infinite-pixels/legokit/commit/9d2ea26))





## [0.1.3](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.2...@legokit/cli@0.1.3) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





## [0.1.2](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.1...@legokit/cli@0.1.2) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





## [0.1.1](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.1.0...@legokit/cli@0.1.1) (2019-08-02)

**Note:** Version bump only for package @legokit/cli





# [0.1.0](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/cli@0.0.6...@legokit/cli@0.1.0) (2019-08-02)


### Features

* remove changelogs from dependencies ([f3a0405](https://gitlab.com/infinite-pixels/legokit/commit/f3a0405))
