module.exports = {
  name: 'build',
  alias: ['b'],
  run: async toolbox => {
    const fs = require('fs-extra');
    const { print, core, webpackConfig, webpackCompilePromise } = toolbox;

    // turns off that loadQuery clutter.
    process.noDeprecation = true;

    // Makes the script crash on unhandled rejections instead of silently
    // ignoring them. In the future, promise rejections that are not handled will
    // terminate the Node.js process with a non-zero exit code.
    process.on('unhandledRejection', err => {
      throw err;
    });

    core.loadEnv({
      NODE_ENV: 'production',
      BABEL_ENV: 'production',
    });

    // Helper function to copy public directory to build/public
    function copyPublicFolder() {
      fs.copySync(core.paths.appPublic, core.paths.appBuildPublic, {
        dereference: true,
        filter: file => file !== core.paths.appHtml,
      });
    }

    fs.emptyDirSync(core.paths.appBuildDist);

    // Merge with the public folder
    copyPublicFolder();

    const clientConfig = webpackConfig('web', 'prod');
    const serverConfig = webpackConfig('node', 'prod');

    try {
      const clientResult = await webpackCompilePromise(clientConfig);
      let message;
      if (clientResult) {
        message = clientResult.toString({
          colors: true,
          children: false,
          chunks: false,
          modules: false,
          moduleTrace: false,
          warningsFilter: () => true,
        });
        print.success(message);
      }
      const serverResult = await webpackCompilePromise(serverConfig);
      if (serverResult) {
        message = serverResult.toString({
          colors: true,
          children: false,
          chunks: false,
          modules: false,
          moduleTrace: false,
          warningsFilter: () => true,
        });
        print.success(message);
      }
    } catch (error) {
      print.error(error); // eslint-disable-line
    }
  },
};
