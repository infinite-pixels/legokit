module.exports = {
  name: 'legokit',
  description: 'Start a legokit app in development mode',
  run: async toolbox => {
    const fs = require('fs-extra');
    const WebpackDevServer = require('webpack-dev-server');
    const detect = require('detect-port');
    const { print, core, webpackConfig, webpackCompile } = toolbox;

    // turns off that loadQuery clutter.
    process.noDeprecation = true;

    let DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3723;
    const PORT = await detect(DEFAULT_PORT);
    if (DEFAULT_PORT !== PORT) {
      DEFAULT_PORT = PORT;
    }

    core.loadEnv({
      NODE_ENV: 'development',
      INSPECT_BRK:
        process.argv.find(arg => arg.match(/--inspect-brk(=|$)/)) || '',
      INSPECT: process.argv.find(arg => arg.match(/--inspect(=|$)/)) || '',
      PORT: DEFAULT_PORT,
    });

    // clean up dist folder on every build
    fs.emptyDirSync(core.paths.appBuildDist);

    const clientConfig = webpackConfig('web', 'dev');
    const serverConfig = webpackConfig('node', 'dev');

    // Compile our assets with webpack
    const clientCompiler = await webpackCompile(clientConfig, false);
    const serverCompiler = await webpackCompile(serverConfig, false);

    serverCompiler.hooks.failed.tap('failed', error => {
      if (error && error.compilation) {
        if (error.compilation.errors && error.compilation.errors.length > 0) {
          print.error('error:', error.compilation.errors); // eslint-disable-line
        }
        if (
          error.compilation.warnings &&
          error.compilation.warnings.length > 0
        ) {
          print.error('error:', error.compilation.warnings); // eslint-disable-line
        }
      }
    });

    // Instatiate a variable to track server watching
    let watching;

    // Start our server webpack instance in watch mode after assets compile
    clientCompiler.hooks.done.tap('done', () => {
      if (watching) {
        return;
      }
      watching = serverCompiler.watch(
        {
          quiet: true,
          stats: 'none',
        },
        /* eslint-disable no-unused-vars */
        stats => {
          if (stats) {
            // You ran Webpack twice. Each instance only supports a single concurrent compilation at a time.
            // console.log('stats==>', stats);
          }
        }
      );
    });
    // Create a new instance of Webpack-dev-server for our client assets.
    // This will actually run on a different port than the users app.
    const clientDevServer = new WebpackDevServer(
      clientCompiler,
      clientConfig.devServer
    );

    // Start Webpack-dev-server
    clientDevServer.listen(DEFAULT_PORT + 1, err => {
      if (err) {
        print.error('clientDevServerError:', err); // eslint-disable-line
      }
    });

    print.info('Starting legokit server...');
  },
};
