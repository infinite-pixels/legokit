module.exports = {
  name: 'test',
  alias: ['t'],
  run: async toolbox => {
    const jest = require('jest');
    const path = require('path');
    const { parameters, core, jestConfig } = toolbox;

    // turns off that loadQuery clutter.
    process.noDeprecation = true;

    // Makes the script crash on unhandled rejections instead of silently
    // ignoring them. In the future, promise rejections that are not handled will
    // terminate the Node.js process with a non-zero exit code.
    process.on('unhandledRejection', err => {
      throw err;
    });

    core.loadEnv({
      NODE_ENV: 'test',
      BABEL_ENV: 'test',
      PUBLIC_URL: '',
    });

    parameters.array.push(
      '--config',
      JSON.stringify(
        jestConfig(
          relativePath => path.resolve(__dirname, '..', relativePath),
          path.resolve(core.paths.appSrc, '..')
        )
      )
    );
    parameters.array.push('--u');
    // parameters.array.push('--coverage');
    jest.run(parameters.array);
  },
};
