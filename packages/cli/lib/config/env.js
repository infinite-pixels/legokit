const fs = require('fs');
const path = require('path');
const paths = require('./index');

// Make sure that including paths.js after env.js will read .env variables.
// delete require.cache[require.resolve('./')]

function loadEnv(envObj) {
  const NODE_ENV = process.env.NODE_ENV || 'development';

  // https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use
  const dotenvFiles = [
    `${paths.env}.${NODE_ENV}.local`,
    `${paths.env}.${NODE_ENV}`,
    // Don't include `.env.local` for `test` environment
    // since normally you expect tests to produce the same
    // results for everyone
    NODE_ENV !== 'test' && `${paths.env}.local`,
    paths.env,
  ];
  // Load environment variables from .env* files. Suppress warnings using silent
  // if this file is missing. dotenv will never modify any environment variables
  // that have already been set.  Variable expansion is supported in .env files.
  // https://github.com/motdotla/dotenv
  // https://github.com/motdotla/dotenv-expand
  dotenvFiles.forEach(dotenvFile => {
    if (fs.existsSync(dotenvFile)) {
      // eslint-disable-next-line
      require('dotenv').config({
        path: dotenvFile,
      });
    }
  });

  process.env = Object.assign(process.env, envObj);
}

// We support resolving modules according to `NODE_PATH`.
// This lets you use absolute paths in imports inside large monorepos:
// https://github.com/facebookincubator/create-react-app/issues/253.
// It works similar to `NODE_PATH` in Node itself:
// https://nodejs.org/api/modules.html#modules_loading_from_the_global_folders
// Note that unlike in Node, only *relative* paths from `NODE_PATH` are honored.
// Otherwise, we risk importing Node.js core modules into an app instead of Webpack shims.
// https://github.com/facebookincubator/create-react-app/issues/1023#issuecomment-265344421
// We also resolve them to make sure all tools using them work consistently.
const appDirectory = fs.realpathSync(process.cwd());
const nodePath = (process.env.NODE_PATH || '')
  .split(path.delimiter)
  .filter(folder => folder && !paths.isAbsolute(folder))
  .map(folder => path.resolve(appDirectory, folder))
  .join(path.delimiter);

// Grab NODE_ENV and LEGOKIT_* environment variables and prepare them to be
// injected into the application via DefinePlugin in Webpack configuration.
const ENV_PREFIX = /^LEGOKIT_/i;

function getClientEnvironment(target) {
  const raw = Object.keys(process.env)
    .filter(key => ENV_PREFIX.test(key))
    .reduce(
      (e, key) => {
        e[key] = process.env[key];
        return e;
      },
      {
        // Useful for determining whether we’re running in production mode.
        // Most importantly, it switches React into the correct mode.
        NODE_ENV: process.env.NODE_ENV || 'development',
        HOST: process.env.HOST || 'localhost',
        PORT: process.env.PORT || 3000,

        // The client sockjs-node is used to listen for code changes.
        WS_PORT: parseInt(process.env.PORT, 10) + 1 || 3000,

        BUILD_TARGET: target === 'web' ? 'client' : 'server',

        // only for production builds. Useful if you need to serve from a CDN
        PUBLIC_PATH: process.env.PUBLIC_PATH || '/',

        // CLIENT_PUBLIC_PATH is a PUBLIC_PATH for NODE_ENV === 'development' && BUILD_TARGET === 'client'
        // It's useful if you're running in a non-localhost container. Ends in a /
        CLIENT_PUBLIC_PATH: process.env.CLIENT_PUBLIC_PATH,

        // Assets path (contains all asset information)
        LEGOKIT_ASSETS_MANIFEST: paths.appManifest,

        // Stats path (useful when loading components dynamically via code splitting)
        LEGOKIT_STATS_MANIFEST: paths.appStatsManifest,

        // The public dir changes between dev and prod, so we use an environment
        // variable available to users.
        LEGOKIT_PUBLIC_DIR:
          process.env.NODE_ENV === 'production'
            ? paths.appBuildPublic
            : paths.appPublic,
      }
    );
  // Stringify all values so we can feed into Webpack DefinePlugin
  const stringified = Object.keys(raw).reduce((e, key) => {
    e[`process.env.${key}`] = JSON.stringify(raw[key]);
    return e;
  }, {});

  return { raw, stringified };
}

module.exports = {
  getClientEnv: getClientEnvironment,
  loadEnv,
  nodePath,
};
