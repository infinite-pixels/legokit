const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const LoadableWebpackPlugin = require('@loadable/webpack-plugin');
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const StartServerPlugin = require('start-server-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

const paths = require('./index');
const devServer = require('./webpack.config.server');
const environment = require('./env');

const eslintPlug = require('../plugs/rule-eslint');
const optimizationPlug = require('../plugs/optimization');
const babelPlug = require('../plugs/rule-babel');
const urlPlug = require('../plugs/rule-url');
const filePlug = require('../plugs/rule-file');

// Webpack uses `publicPath` to determine where the app is being served from.
// It requires a trailing slash, or the file assets will get an incorrect path.
// const publicPath = paths.servedPath;
// Some apps do not use client-side routing with pushState.
// For these, "homepage" can be set to "." to enable relative asset paths.
// const shouldUseRelativeAssetPaths = publicPath === './';

module.exports = (target = 'web', env = 'dev') => {
  const IS_NODE = target === 'node';
  const IS_WEB = target === 'web';
  const IS_PROD = env === 'prod';
  const IS_DEV = env === 'dev';

  //   process.env.NODE_ENV = IS_PROD ? 'production' : 'development'
  const dotenv = environment.getClientEnv(target);
  let conf = {
    mode: IS_DEV ? 'development' : 'production',
    // Set webpack context to the current command's directory
    context: process.cwd(),
    // Specify target (either 'node' or 'web')
    target,
    plugins: [],
    optimization: {},
    module: {
      strictExportPresence: true,
      rules: [],
    },
    // We need to tell webpack how to resolve both APP's node_modules and
    // the users', so we use resolve and resolveLoader.
    resolve: {
      modules: ['node_modules', paths.appNodeModules].concat(
        // It is guaranteed to exist because we tweak it in `env.js`
        environment.nodePath.split(path.delimiter).filter(Boolean)
      ),
      extensions: ['.mjs', '.jsx', '.js', '.json'],
      alias: {
        // This is required so symlinks work during development.
        'webpack/hot/poll': require.resolve('webpack/hot/poll'),
      },
    },
    // Turn off performance processing because we utilize
    // our own hints via the FileSizeReporter
    performance: false,
  };

  if (IS_PROD) {
    conf.bail = true;
  }

  if (IS_DEV) {
    // You may want 'eval' instead if you prefer to see the compiled output in DevTools.
    // See the discussion in https://github.com/facebook/create-react-app/issues/343
    // conf.devtool = 'cheap-module-source-map';
  } else {
    // Don't attempt to continue if there are any errors.
    // We generate sourcemaps in production. This is slow but gives good results.
    // You can exclude the *.map files from the build during deployment.
    // conf.devtool = shouldUseSourceMap ? 'source-map' : false;
  }

  conf.resolveLoader = {
    modules: [paths.appNodeModules, paths.ownNodeModules],
  };

  // =============================================
  // Disable require.ensure as it's not a standard language feature.
  conf.module.rules.push({ parser: { requireEnsure: false } });

  const optionConf = { target, env, dev: IS_DEV, ...paths };
  conf = eslintPlug(conf, optionConf); // eslint-disable-line
  conf = urlPlug(conf, optionConf); // eslint-disable-line
  conf = babelPlug(conf, optionConf); // eslint-disable-line
  conf = filePlug(conf, optionConf); // eslint-disable-line

  // We define environment variables that can be accessed globally in our
  conf.plugins.push(new webpack.DefinePlugin(dotenv.stringified));

  conf.plugins.push(
    new SimpleProgressWebpackPlugin({
      format: 'minimal',
      name: target === 'web' ? 'Client:' : 'Server:',
    })
  );

  const devServerPort = parseInt(dotenv.raw.PORT, 10) + 1;
  if (IS_NODE) {
    conf.entry = [paths.appServerIndexJs];
    conf.node = {
      __console: false,
      __dirname: false,
      __filename: false,
    };
    // We need to tell webpack what to bundle into our Node bundle.
    conf.externals = [
      nodeExternals({
        whitelist: [
          IS_DEV ? 'webpack/hot/poll?300' : null,
          /\.(eot|woff|woff2|ttf|otf)$/,
          /\.(svg|png|jpg|jpeg|gif|ico)$/,
          /\.(mp4|mp3|ogg|swf|webp)$/,
          /\.(css|scss|sass|sss|less)$/,
        ].filter(x => x),
      }),
    ];

    // Specify webpack Node.js output path and filename
    conf.output = {
      path: paths.appBuildDist,
      publicPath: IS_DEV ? `http://${dotenv.raw.HOST}:${devServerPort}/` : '/',
      filename: 'server.js',
      libraryTarget: 'commonjs2',
    };
    // Prevent creating multiple chunks for the server
    conf.plugins.push(
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1,
      })
    );
    if (IS_DEV) {
      // Use watch mode
      conf.watch = true;
      // https://github.com/webpack/docs/issues/45#issuecomment-149793458
      // Use webpack/hot/poll or webpack/hot/signal
      // The first polls the fs for updates(easy to use)
      // The second listens for a process event to check for updates(you need a way to send the signal)
      conf.entry.unshift('webpack/hot/poll?300');
      // Add hot module replacement
      conf.plugins.push(new webpack.HotModuleReplacementPlugin());

      const nodeArgs = ['-r', 'source-map-support/register'];
      // Passthrough --inspect and --inspect-brk flags (with optional [host:port] value) to node
      if (process.env.INSPECT_BRK) {
        nodeArgs.push(process.env.INSPECT_BRK);
      } else if (process.env.INSPECT) {
        nodeArgs.push(process.env.INSPECT);
      }
      // Automatically start your server once Webpack's build completes.
      conf.plugins.push(
        new StartServerPlugin({
          name: 'server.js',
          nodeArgs,
        })
      );

      // Ignore assets.json to avoid infinite recompile bug
      conf.plugins.push(new webpack.WatchIgnorePlugin([paths.appManifest]));
    }
  }

  if (IS_WEB) {
    // Output our JS and CSS files in a manifest file called assets.json
    // in the build directory.
    conf.plugins.push(new HardSourceWebpackPlugin());

    conf.plugins.push(
      new LoadableWebpackPlugin({
        filename: '../stats.json',
        writeToDisk: true,
      })
    );

    if (IS_DEV) {
      // Setup Webpack Dev Server on port 3001 and
      // specify our client entry point /client/index.js
      conf.entry = {
        client: [
          require.resolve('webpack-hot-dev-clients/webpackHotDevClient'),
          paths.appClientIndexJs,
        ].filter(Boolean),
      };
      // Configure our client bundles output. Not the public path is to 3001.
      conf.output = {
        path: paths.appBuildPublic,
        publicPath: `http://${dotenv.raw.HOST}:${devServerPort}/`,
        pathinfo: true,
        libraryTarget: 'var',
        filename: 'static/js/bundle.js',
        chunkFilename: 'static/js/[name].chunk.js',
        devtoolModuleFilenameTemplate: info =>
          path.resolve(info.resourcePath).replace(/\\/g, '/'),
      };

      conf.plugins.push(new webpack.HotModuleReplacementPlugin());

      // Configure webpack-dev-server to serve our client-side bundle from
      // http://${dotenv.raw.HOST}:3001
      conf.devServer = devServer(dotenv);
    } else {
      // Specify production entry point (/client/index.js)
      conf.entry = {
        client: paths.appClientIndexJs,
      };
      // Specify the client output directory and paths. Notice that we have
      // changed the publiPath to just '/' from http://localhost:3001. This is because
      // we will only be using one port in production.
      conf.output = {
        path: paths.appBuildPublic,
        publicPath: dotenv.raw.PUBLIC_PATH || '/',
        filename: 'static/js/bundle.[chunkhash:8].js',
        chunkFilename: 'static/js/[chunkhash:8].chunk.js',
        libraryTarget: 'var',
      };

      conf.plugins.push(new CompressionPlugin());

      conf = optimizationPlug(conf, { target, env }); // eslint-disable-line
    }

    // Watcher doesn't work well if you mistype casing in a path so we use
    // a plugin that prints an error when you attempt to do this.
    // See https://github.com/facebook/create-react-app/issues/240
    conf.plugins.push(new CaseSensitivePathsPlugin());
  }

  // Moment.js is an extremely popular library that bundles large locale files
  // by default due to how Webpack interprets its code. This is a practical
  // solution that requires the user to opt into importing specific locales.
  // https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
  // You can remove this if you don't use Moment.js:
  conf.plugins.push(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/));

  conf.plugins.filter(Boolean);
  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  conf.node = {
    ...conf.node,
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  };

  return conf;
};
