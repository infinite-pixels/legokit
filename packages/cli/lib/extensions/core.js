// add your CLI-specific functionality here, which will then be accessible
// to your commands
const paths = require('../config');
const { loadEnv } = require('../config/env');

module.exports = toolbox => {
  toolbox.core = {
    paths,
    loadEnv,
  };
};
