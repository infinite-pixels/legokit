const jestConfig = require('../config/jest');

module.exports = toolbox => {
  toolbox.jestConfig = jestConfig;

  // enable this if you want to read configuration in from
  // the current folder's package.json (in a "cli" property),
  // cli.config.json, etc.
  // toolbox.config = {
  //   ...toolbox.config,
  //   ...toolbox.config.loadConfig(process.cwd(), "cli")
  // }
};
