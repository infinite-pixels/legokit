// add your CLI-specific functionality here, which will then be accessible
// to your commands
// const paths = require('../config');
const webpack = require('webpack');
const createConfig = require('../config/webpack.config');

// Wrap webpack compile in a try catch.
// Webpack compile in a try-catch
function compile(config) {
  let compiler;
  try {
    compiler = webpack(config);
  } catch (e) {
    console.log('Failed to compile: ', [e]); // eslint-disable-line
    process.exit(1);
  }
  return compiler;
}

function compilePromise(config) {
  return new Promise((resolve, reject) => {
    let compiler;
    try {
      compiler = webpack(config);
    } catch (e) {
      print.error('compile errors:', [e]); // eslint-disable-line
      reject(e);
      process.exit(1);
    }

    compiler.run((err, stats) => {
      err ? reject(err) : resolve(stats);
    });
  });
}

module.exports = toolbox => {
  toolbox.webpackConfig = createConfig;
  toolbox.webpackCompile = compile;
  toolbox.webpackCompilePromise = compilePromise;
};
