// Process application JS with Babel.
// The preset includes JSX, Flow, TypeScript and some ESnext features.
module.exports = (conf, { appSrc }) => {
  const mainBabelOptions = {
    babelrc: true,
    cacheDirectory: true,
    presets: ['babel-preset-legokit'],
  };

  conf.module.rules = [
    ...conf.module.rules,
    {
      test: /\.(js|mjs|jsx|ts|tsx)$/,
      include: appSrc,
      use: [
        {
          loader: require.resolve('babel-loader'),
          options: mainBabelOptions,
        },
      ],
    },
  ];
  return conf;
};
