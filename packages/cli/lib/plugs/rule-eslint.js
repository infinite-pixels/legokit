// First, run the linter.
// It's important to do this before Babel processes the JS.
const path = require('path');
const eslint = require('eslint');
const paths = require('../config');

module.exports = (conf, options) => {
  conf.module.rules = [
    ...conf.module.rules,
    {
      test: /\.(js|mjs|jsx)$/,
      include: options.appSrc,
      enforce: 'pre',
      use: [
        {
          options: {
            eslintPath: require.resolve('eslint'),
            resolvePluginsRelativeTo: __dirname,
            // @remove-on-eject-begin
            baseConfig: (() => {
              const eslintCli = new eslint.CLIEngine();
              let eslintConfig;
              try {
                eslintConfig = eslintCli.getConfigForFile(
                  path.resolve(paths.appSrc, 'index.js')
                );
              } catch (e) {
                // A config couldn't be found.
              }

              // We allow overriding the config, only if it extends our config
              // (`extends` can be a string or array of strings).
              if (
                eslintConfig &&
                eslintConfig.extends &&
                eslintConfig.extends.includes('eslint-config-legokit')
              ) {
                return eslintConfig;
              } else {
                return {
                  extends: [require.resolve('eslint-config-legokit')],
                };
              }
            })(),
            ignore: false,
            useEslintrc: false,
          },
          loader: require.resolve('eslint-loader'),
        },
      ],
    },
  ];
  return conf;
};
