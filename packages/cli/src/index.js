import path from 'path';
import { build as builder } from 'gluegun';

/**
 * Create the cli and kick it off
 */
async function run(argv) {
  // create a CLI runtime
  const cli = builder()
    .brand('legokit')
    .src(path.resolve(__dirname, '../lib'))
    .help()
    .version()
    .create();

  // and run it
  const toolbox = await cli.run(argv);

  // send it back (for testing, mostly)
  return toolbox;
}

export default run;
