# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.211](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.214...@legokit/core@1.0.211) (2019-08-13)

**Note:** Version bump only for package @legokit/core





## [1.0.214](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.213...@legokit/core@1.0.214) (2019-08-13)

**Note:** Version bump only for package @legokit/core





## [1.0.213](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.212...@legokit/core@1.0.213) (2019-08-13)

**Note:** Version bump only for package @legokit/core





## [1.0.212](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.211...@legokit/core@1.0.212) (2019-08-13)

**Note:** Version bump only for package @legokit/core





## [1.0.211](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.210...@legokit/core@1.0.211) (2019-08-13)

**Note:** Version bump only for package @legokit/core





## [1.0.210](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.209...@legokit/core@1.0.210) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.209](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.208...@legokit/core@1.0.209) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.208](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.207...@legokit/core@1.0.208) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.207](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.206...@legokit/core@1.0.207) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.206](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.205...@legokit/core@1.0.206) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.205](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.204...@legokit/core@1.0.205) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.204](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.203...@legokit/core@1.0.204) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.203](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.202...@legokit/core@1.0.203) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.202](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.201...@legokit/core@1.0.202) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.201](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.200...@legokit/core@1.0.201) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.200](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.199...@legokit/core@1.0.200) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.199](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.198...@legokit/core@1.0.199) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.198](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.197...@legokit/core@1.0.198) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.197](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.196...@legokit/core@1.0.197) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.196](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.195...@legokit/core@1.0.196) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.195](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.194...@legokit/core@1.0.195) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.194](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.193...@legokit/core@1.0.194) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.193](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.192...@legokit/core@1.0.193) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.192](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.191...@legokit/core@1.0.192) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.191](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.190...@legokit/core@1.0.191) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.190](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.189...@legokit/core@1.0.190) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.189](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.188...@legokit/core@1.0.189) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.188](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.187...@legokit/core@1.0.188) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.187](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.186...@legokit/core@1.0.187) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.186](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.185...@legokit/core@1.0.186) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.185](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.184...@legokit/core@1.0.185) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.184](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.183...@legokit/core@1.0.184) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.183](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.182...@legokit/core@1.0.183) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.182](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.181...@legokit/core@1.0.182) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.181](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.180...@legokit/core@1.0.181) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.180](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.179...@legokit/core@1.0.180) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.179](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.178...@legokit/core@1.0.179) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.178](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.177...@legokit/core@1.0.178) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.177](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.176...@legokit/core@1.0.177) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.176](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.175...@legokit/core@1.0.176) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.175](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.174...@legokit/core@1.0.175) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.174](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.173...@legokit/core@1.0.174) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.173](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.172...@legokit/core@1.0.173) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.172](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.171...@legokit/core@1.0.172) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.171](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.170...@legokit/core@1.0.171) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.170](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.169...@legokit/core@1.0.170) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.169](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.168...@legokit/core@1.0.169) (2019-08-12)

**Note:** Version bump only for package @legokit/core





## [1.0.168](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.167...@legokit/core@1.0.168) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.167](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.166...@legokit/core@1.0.167) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.166](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.165...@legokit/core@1.0.166) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.165](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.164...@legokit/core@1.0.165) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.164](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.163...@legokit/core@1.0.164) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.163](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.162...@legokit/core@1.0.163) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.162](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.161...@legokit/core@1.0.162) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.161](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.160...@legokit/core@1.0.161) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.160](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.159...@legokit/core@1.0.160) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.159](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.158...@legokit/core@1.0.159) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.158](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.157...@legokit/core@1.0.158) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.157](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.156...@legokit/core@1.0.157) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.156](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.155...@legokit/core@1.0.156) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.155](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.154...@legokit/core@1.0.155) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.154](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.153...@legokit/core@1.0.154) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.153](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.153...@legokit/core@1.0.153) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.153](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.152...@legokit/core@1.0.153) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.152](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.151...@legokit/core@1.0.152) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.151](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.150...@legokit/core@1.0.151) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.150](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.149...@legokit/core@1.0.150) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.149](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.148...@legokit/core@1.0.149) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.148](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.147...@legokit/core@1.0.148) (2019-08-11)

**Note:** Version bump only for package @legokit/core





## [1.0.147](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.146...@legokit/core@1.0.147) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.146](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.145...@legokit/core@1.0.146) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.145](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.144...@legokit/core@1.0.145) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.144](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.143...@legokit/core@1.0.144) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.143](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.142...@legokit/core@1.0.143) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.142](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.141...@legokit/core@1.0.142) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.141](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.140...@legokit/core@1.0.141) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.140](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.139...@legokit/core@1.0.140) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.139](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.138...@legokit/core@1.0.139) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.138](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.137...@legokit/core@1.0.138) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.137](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.136...@legokit/core@1.0.137) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.136](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.135...@legokit/core@1.0.136) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.135](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.134...@legokit/core@1.0.135) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.134](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.133...@legokit/core@1.0.134) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.133](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.132...@legokit/core@1.0.133) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.132](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.131...@legokit/core@1.0.132) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.131](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.130...@legokit/core@1.0.131) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.130](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.129...@legokit/core@1.0.130) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.129](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.128...@legokit/core@1.0.129) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.128](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.127...@legokit/core@1.0.128) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.127](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.126...@legokit/core@1.0.127) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.126](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.125...@legokit/core@1.0.126) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.125](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.124...@legokit/core@1.0.125) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.124](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.123...@legokit/core@1.0.124) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.123](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.122...@legokit/core@1.0.123) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.122](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.121...@legokit/core@1.0.122) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.121](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.120...@legokit/core@1.0.121) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.120](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.119...@legokit/core@1.0.120) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.119](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.118...@legokit/core@1.0.119) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.118](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.117...@legokit/core@1.0.118) (2019-08-09)

**Note:** Version bump only for package @legokit/core





## [1.0.117](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.116...@legokit/core@1.0.117) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.116](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.115...@legokit/core@1.0.116) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.115](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.114...@legokit/core@1.0.115) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.114](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.113...@legokit/core@1.0.114) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.113](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.112...@legokit/core@1.0.113) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.112](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.111...@legokit/core@1.0.112) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.111](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.110...@legokit/core@1.0.111) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.110](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.109...@legokit/core@1.0.110) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.109](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.108...@legokit/core@1.0.109) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.108](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.107...@legokit/core@1.0.108) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.107](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.106...@legokit/core@1.0.107) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.106](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.105...@legokit/core@1.0.106) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.105](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.104...@legokit/core@1.0.105) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.104](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.103...@legokit/core@1.0.104) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.103](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.102...@legokit/core@1.0.103) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.102](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.101...@legokit/core@1.0.102) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.101](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.100...@legokit/core@1.0.101) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.100](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.99...@legokit/core@1.0.100) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.99](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.98...@legokit/core@1.0.99) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.98](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.97...@legokit/core@1.0.98) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.97](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.96...@legokit/core@1.0.97) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.96](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.95...@legokit/core@1.0.96) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.95](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.94...@legokit/core@1.0.95) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.94](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.93...@legokit/core@1.0.94) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.93](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.92...@legokit/core@1.0.93) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.92](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.91...@legokit/core@1.0.92) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.91](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.90...@legokit/core@1.0.91) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.90](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.89...@legokit/core@1.0.90) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.89](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.88...@legokit/core@1.0.89) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.88](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.87...@legokit/core@1.0.88) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.87](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.86...@legokit/core@1.0.87) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.86](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.85...@legokit/core@1.0.86) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.85](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.84...@legokit/core@1.0.85) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.84](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.83...@legokit/core@1.0.84) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.83](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.82...@legokit/core@1.0.83) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.82](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.81...@legokit/core@1.0.82) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.81](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.80...@legokit/core@1.0.81) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.80](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.79...@legokit/core@1.0.80) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.79](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.78...@legokit/core@1.0.79) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.78](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.77...@legokit/core@1.0.78) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.77](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.76...@legokit/core@1.0.77) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.76](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.75...@legokit/core@1.0.76) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.75](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.74...@legokit/core@1.0.75) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.74](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.73...@legokit/core@1.0.74) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.73](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.72...@legokit/core@1.0.73) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.72](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.71...@legokit/core@1.0.72) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.71](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.70...@legokit/core@1.0.71) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.70](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.69...@legokit/core@1.0.70) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.69](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.68...@legokit/core@1.0.69) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.68](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.67...@legokit/core@1.0.68) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.67](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.66...@legokit/core@1.0.67) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.66](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.65...@legokit/core@1.0.66) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.65](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.64...@legokit/core@1.0.65) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.64](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.63...@legokit/core@1.0.64) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.63](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.62...@legokit/core@1.0.63) (2019-08-07)

**Note:** Version bump only for package @legokit/core





## [1.0.62](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.61...@legokit/core@1.0.62) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.61](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.60...@legokit/core@1.0.61) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.60](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.59...@legokit/core@1.0.60) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.59](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.58...@legokit/core@1.0.59) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.58](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.57...@legokit/core@1.0.58) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.57](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.56...@legokit/core@1.0.57) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.56](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.55...@legokit/core@1.0.56) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.55](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.54...@legokit/core@1.0.55) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.54](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.53...@legokit/core@1.0.54) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.53](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.52...@legokit/core@1.0.53) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.52](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.51...@legokit/core@1.0.52) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.51](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.50...@legokit/core@1.0.51) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.50](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.49...@legokit/core@1.0.50) (2019-08-06)


### Bug Fixes

* DocRoot issue ([78443de](https://gitlab.com/infinite-pixels/legokit/commit/78443de))





## [1.0.49](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.48...@legokit/core@1.0.49) (2019-08-06)


### Bug Fixes

* module imports ([caab2b1](https://gitlab.com/infinite-pixels/legokit/commit/caab2b1))





## [1.0.48](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.47...@legokit/core@1.0.48) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.47](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.46...@legokit/core@1.0.47) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.46](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.45...@legokit/core@1.0.46) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.45](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.44...@legokit/core@1.0.45) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.44](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.43...@legokit/core@1.0.44) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.43](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.42...@legokit/core@1.0.43) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.42](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.41...@legokit/core@1.0.42) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.41](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.40...@legokit/core@1.0.41) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.40](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.39...@legokit/core@1.0.40) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.39](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.38...@legokit/core@1.0.39) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.38](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.37...@legokit/core@1.0.38) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.37](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.36...@legokit/core@1.0.37) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.36](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.35...@legokit/core@1.0.36) (2019-08-06)

**Note:** Version bump only for package @legokit/core





## [1.0.35](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.34...@legokit/core@1.0.35) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.34](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.33...@legokit/core@1.0.34) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.33](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.32...@legokit/core@1.0.33) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.32](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.31...@legokit/core@1.0.32) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.31](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.30...@legokit/core@1.0.31) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.30](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.29...@legokit/core@1.0.30) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.29](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.28...@legokit/core@1.0.29) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.28](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.27...@legokit/core@1.0.28) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.27](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.26...@legokit/core@1.0.27) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.26](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.25...@legokit/core@1.0.26) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.25](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.24...@legokit/core@1.0.25) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.24](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.23...@legokit/core@1.0.24) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.23](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.22...@legokit/core@1.0.23) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.22](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.21...@legokit/core@1.0.22) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.21](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.20...@legokit/core@1.0.21) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.20](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.19...@legokit/core@1.0.20) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.19](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.18...@legokit/core@1.0.19) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.18](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.17...@legokit/core@1.0.18) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.17](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.16...@legokit/core@1.0.17) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.16](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.15...@legokit/core@1.0.16) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.15](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.14...@legokit/core@1.0.15) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.14](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.13...@legokit/core@1.0.14) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.13](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.12...@legokit/core@1.0.13) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.12](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.11...@legokit/core@1.0.12) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.11](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.10...@legokit/core@1.0.11) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.10](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.9...@legokit/core@1.0.10) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.9](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.8...@legokit/core@1.0.9) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.8](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.7...@legokit/core@1.0.8) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.7](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.6...@legokit/core@1.0.7) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.6](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.5...@legokit/core@1.0.6) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.5](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.4...@legokit/core@1.0.5) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.4](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.3...@legokit/core@1.0.4) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.3](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.2...@legokit/core@1.0.3) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## [1.0.2](https://gitlab.com/infinite-pixels/legokit/compare/@legokit/core@1.0.1...@legokit/core@1.0.2) (2019-08-05)

**Note:** Version bump only for package @legokit/core





## 1.0.1 (2019-08-05)

**Note:** Version bump only for package @legokit/core
