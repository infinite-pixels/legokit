import React, { Component } from 'react';
import serialize from 'serialize-javascript';

export function DocumentRoot() {
  return <div id="root">__LEGOKIT__</div>;
}

export function DocumentData({ name, data }) {
  return (
    <script
      id="initial-app-data"
      type="text/javascript"
      dangerouslySetInnerHTML={{
        __html: `window.${name}=${serialize({ ...data })}`,
      }}
    />
  );
}

export class Document extends Component {
  static async getInitialProps({ extractor, data, store, renderPage }) {
    const page = await renderPage();
    return { extractor, data, store, ...page };
  }

  render() {
    const { helmet, extractor, store } = this.props;
    // get attributes from React Helmet
    const htmlAttrs = helmet.htmlAttributes.toComponent();
    const bodyAttrs = helmet.bodyAttributes.toComponent();

    return (
      <html {...htmlAttrs}>
        <head>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta charSet="utf-8" />
          <title>Welcome to Legokit.</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          {helmet.title.toComponent()}
          {helmet.meta.toComponent()}
          {helmet.link.toComponent()}
          {extractor.getLinkElements()}
          {extractor.getStyleElements()}
        </head>
        <body {...bodyAttrs}>
          <DocumentRoot />
          {store && store.getState && (
            <DocumentData
              name="__INITIAL_STORE_DATA__"
              data={store.getState()}
            />
          )}
          {extractor.getScriptElements()}
        </body>
      </html>
    );
  }
}
