import React from 'react';
import { hydrate } from 'react-dom';
import { loadableReady } from '@loadable/component';
import { BrowserRouter } from 'react-router-dom';

import { Lego } from './lego';

export default function loadReady(routes, nav) {
  loadableReady(() => {
    hydrate(
      <BrowserRouter>
        <Lego nav={nav} routes={routes} />
      </BrowserRouter>,
      document.getElementById('root')
    );
  });
}
