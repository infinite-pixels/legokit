import { Lego } from './lego';
import loadReady from './ensureReady';
import { render } from './render';
import { Document, DocumentRoot, DocumentData } from './document';
import loadable from '@loadable/component';
import { ChunkExtractor } from '@loadable/server';

module.exports = {
  Lego,
  render,
  loadable,
  loadReady,
  ChunkExtractor,
  Document,
  DocumentData,
  DocumentRoot,
};
