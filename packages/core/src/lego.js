import React, { Fragment } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';

function Legokit({ routes, location, nav: Navigation = null }) {
  return (
    <Fragment>
      <Navigation />
      <main>
        <Switch>
          {routes.map((route, idx) => (
            <Route
              key={idx}
              path={route.path}
              exact={route.exact}
              location={location}
              render={props => {
                return React.createElement(route.component, {
                  history: props.history,
                  location,
                  match: props.match,
                });
              }}
            />
          ))}
        </Switch>
      </main>
    </Fragment>
  );
}

export const Lego = withRouter(Legokit);
