import React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import Helmet from 'react-helmet';
import { matchPath, StaticRouter } from 'react-router-dom';

import { Document as DefaultDoc } from './document';
import { Lego } from './lego';

export async function render(options) {
  const {
    req,
    res,
    routes,
    nav,
    extractor,
    document: Document,
    renderer: customRenderer,
    ...rest
  } = options;
  const Doc = Document || DefaultDoc;
  const context = rest.context || {};

  const renderPage = async () => {
    // By default, we keep ReactDOMServer synchronous renderToString function
    const defaultRenderer = element => ({
      html: renderToString(extractor.collectChunks(element)),
    });
    const renderer = customRenderer || defaultRenderer;
    const asyncOrSyncRender = renderer(
      <StaticRouter location={req.req.url} context={context}>
        <Lego nav={nav} routes={routes} />
      </StaticRouter>
    );

    const renderedContent = await asyncOrSyncRender;
    const helmet = Helmet.renderStatic();

    return { helmet, ...renderedContent };
  };

  const match = routes.find(route => {
    return !!matchPath(req.req.url, route);
  });

  if (!match) {
    res.status(404);
    return;
  }

  if (match.path === '**') {
    res.status(404);
  } else if (match && match.redirectTo && match.path) {
    res.redirect(
      301,
      req.req.originalUrl.replace(match.path, match.redirectTo)
    );
    return;
  }

  const { html, ...docProps } = await Doc.getInitialProps({
    req,
    res,
    extractor,
    renderPage,
    helmet: Helmet.renderStatic(),
    match,
    ...rest,
  });

  const doc = renderToStaticMarkup(<Doc {...docProps} />);
  return `<!doctype html>${doc.replace('__LEGOKIT__', html)}`;
}
