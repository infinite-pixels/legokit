export const isFunction = obj => 'function' === typeof obj;

export const isObject = obj => obj !== null && typeof obj === 'object';

export const isPromise = value => isObject(value) && isFunction(value.then);
