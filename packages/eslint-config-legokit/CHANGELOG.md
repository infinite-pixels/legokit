# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.20](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.19...eslint-config-legokit@1.0.20) (2019-08-11)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.19](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.19...eslint-config-legokit@1.0.19) (2019-08-11)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.19](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.18...eslint-config-legokit@1.0.19) (2019-08-11)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.18](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.17...eslint-config-legokit@1.0.18) (2019-08-05)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.17](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.16...eslint-config-legokit@1.0.17) (2019-08-05)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.16](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.15...eslint-config-legokit@1.0.16) (2019-08-05)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.15](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.14...eslint-config-legokit@1.0.15) (2019-08-05)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.14](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.13...eslint-config-legokit@1.0.14) (2019-08-04)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.13](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.12...eslint-config-legokit@1.0.13) (2019-08-04)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.12](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.11...eslint-config-legokit@1.0.12) (2019-08-04)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.11](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.10...eslint-config-legokit@1.0.11) (2019-08-04)


### Bug Fixes

* hopefully ([adeeca5](https://gitlab.com/infinite-pixels/legokit/commit/adeeca5))





## [1.0.10](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.9...eslint-config-legokit@1.0.10) (2019-08-04)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.9](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.8...eslint-config-legokit@1.0.9) (2019-08-04)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.8](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.7...eslint-config-legokit@1.0.8) (2019-08-04)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.7](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.6...eslint-config-legokit@1.0.7) (2019-08-03)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.6](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.5...eslint-config-legokit@1.0.6) (2019-08-03)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.5](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.4...eslint-config-legokit@1.0.5) (2019-08-03)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.4](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.3...eslint-config-legokit@1.0.4) (2019-08-03)


### Reverts

* back to preconstruct ([d86df4f](https://gitlab.com/infinite-pixels/legokit/commit/d86df4f))





## [1.0.3](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.2...eslint-config-legokit@1.0.3) (2019-08-03)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.2](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.0.1...eslint-config-legokit@1.0.2) (2019-08-03)

**Note:** Version bump only for package eslint-config-legokit





## [1.0.1](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.1.1...eslint-config-legokit@1.0.1) (2019-08-03)

**Note:** Version bump only for package eslint-config-legokit





## [1.1.1](https://gitlab.com/infinite-pixels/legokit/compare/eslint-config-legokit@1.1.0...eslint-config-legokit@1.1.1) (2019-08-03)

**Note:** Version bump only for package eslint-config-legokit





# 1.1.0 (2019-08-03)


### Features

* add custom eslint config module ([13ddb56](https://gitlab.com/infinite-pixels/legokit/commit/13ddb56))
